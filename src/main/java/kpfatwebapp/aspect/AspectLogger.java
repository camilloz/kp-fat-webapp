package kpfatwebapp.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class AspectLogger {
	private Logger logger = Logger.getLogger(AspectLogger.class);

    @Before("execution(* kpfatwebapp.service.UserService.*(..))")
    public void logBeforeSavingUser (JoinPoint joinPoint) 
    {
    	logger.info("[AspectOrientedProgramming Logger] Will be executing method: " + joinPoint.getSignature().getName());
    }
}