package kpfatwebapp.entity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "FILM_SHOW")
public class FilmShow {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FILM_SHOW_ID")
	private Integer id;

	@NotEmpty
	@Column(name = "TITLE")
	private String title;

	@NotEmpty
	@Column(name = "DIRECTOR_NAME")
	private String directorName;
	
	@NotNull
	@Column(name = "DATE")
	private Date date;

	@NotNull
	@Column(name = "DURATION")
	private Integer duration;

	@NotNull
	@Column(name = "NUMBER_OF_TICKETS")
	private Integer numberOfTicketsAvailable;

	@NotNull
	@Column(name = "TICKET_PRICE")
	private Double ticketPrice;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDirectorName() {
		return directorName;
	}

	public void setDirectorName(String directorName) {
		this.directorName = directorName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(String date) throws ParseException {
	    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm"); 
	    this.date = df.parse(date);
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getNumberOfTicketsAvailable() {
		return numberOfTicketsAvailable;
	}

	public void setNumberOfTicketsAvailable(Integer numberOfTicketsAvailable) {
		this.numberOfTicketsAvailable = numberOfTicketsAvailable;
	}

	public Double getTicketPrice() {
		return ticketPrice;
	}

	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((directorName == null) ? 0 : directorName.hashCode());
		result = prime * result + ((duration == null) ? 0 : duration.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((numberOfTicketsAvailable == null) ? 0 : numberOfTicketsAvailable.hashCode());
		result = prime * result + ((ticketPrice == null) ? 0 : ticketPrice.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FilmShow other = (FilmShow) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (directorName == null) {
			if (other.directorName != null)
				return false;
		} else if (!directorName.equals(other.directorName))
			return false;
		if (duration == null) {
			if (other.duration != null)
				return false;
		} else if (!duration.equals(other.duration))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (numberOfTicketsAvailable == null) {
			if (other.numberOfTicketsAvailable != null)
				return false;
		} else if (!numberOfTicketsAvailable.equals(other.numberOfTicketsAvailable))
			return false;
		if (ticketPrice == null) {
			if (other.ticketPrice != null)
				return false;
		} else if (!ticketPrice.equals(other.ticketPrice))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
