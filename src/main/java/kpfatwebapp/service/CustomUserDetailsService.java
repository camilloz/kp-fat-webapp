package kpfatwebapp.service;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;

import kpfatwebapp.aspect.AspectLogger;
import kpfatwebapp.entity.Role;
import kpfatwebapp.entity.User;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
	private Logger logger = Logger.getLogger(AspectLogger.class);
	
	@Autowired
	private UserService userService;

	@Transactional(readOnly = true)
	@Override
	public UserDetails loadUserByUsername(@Valid @ModelAttribute("email") String email) throws UsernameNotFoundException {
		User user = userService.findByEmail(email);
		if (user == null) {
			logger.error("User not found");
			throw new UsernameNotFoundException("Username not found");
		}
		logger.info("LOGGING User : " + user.getFirstName() + " "+ user.getLastName());
		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), true, true,
				true, true, getGrantedAuthorities(user));
	}

	private List<GrantedAuthority> getGrantedAuthorities(User user) {
		List<GrantedAuthority> authorities = new ArrayList<>();

		for (Role role : user.getRoles()) {
			authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
		}
		return authorities;
	}
}
