package kpfatwebapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kpfatwebapp.dao.FilmShowDao;
import kpfatwebapp.entity.FilmShow;

@Service("filmshowService")
@Transactional
public class FilmShowService {

	@Autowired
	private FilmShowDao filmShowDao;

	public FilmShow findById(int id) {
		return filmShowDao.findById(id);
	}

	public void save(FilmShow filmShow) {
		filmShowDao.save(filmShow);
	}
	
	public void sellTickets (Integer id, Integer numberOfTickets) {
		filmShowDao.sellTickets(id, numberOfTickets);
	}
	
	public List<FilmShow> findAll() {
		return filmShowDao.findAll();
	}
}