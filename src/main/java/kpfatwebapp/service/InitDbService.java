package kpfatwebapp.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kpfatwebapp.dao.RoleDao;

@Service("initDbService")
@Transactional
public class InitDbService {
	@Autowired
	private RoleDao roleDao;
	
	@PostConstruct
	public void init() {
		roleDao.populate();
	}
}
