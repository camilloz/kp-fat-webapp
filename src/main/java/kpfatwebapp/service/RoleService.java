package kpfatwebapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kpfatwebapp.dao.RoleDao;
import kpfatwebapp.entity.Role;

@Service("roleService")
@Transactional
public class RoleService {

	@Autowired
	RoleDao roleDao;

	public Role findById(int id) {
		return roleDao.findById(id);
	}
}