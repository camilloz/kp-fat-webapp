package kpfatwebapp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kpfatwebapp.dao.RoleDao;
import kpfatwebapp.dao.UserDao;
import kpfatwebapp.entity.Role;
import kpfatwebapp.entity.User;

@Service("userService")
@Transactional
public class UserService {

	@Autowired
	private UserDao userDao;
	@Autowired
	private RoleDao roleDao;
	@Autowired
	PasswordEncoder passwordEncoder;

	public User findById(int id) {
		return userDao.findById(id);
	}

	public User findByEmail(String email) {
		return userDao.findByEmail(email);
	}

	public void save(User user) {
		if (isUserEmailUnique(user.getEmail())) {
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			List<Role> roles = new ArrayList<>();
			roles.add(roleDao.findByName("USER"));
			user.setRoles(roles);
			userDao.save(user);
		}
	}

	public void updateUser(User user) {
		User entity = userDao.findById(user.getId());
		if (entity != null) {
			entity.setFirstName(user.getFirstName());
			entity.setLastName(user.getLastName());
			entity.setEmail(user.getEmail());
		}
	}
	
	 public boolean isUserEmailUnique(String email) {
		 User user = findByEmail(email);
		 return user == null;
	 }
}