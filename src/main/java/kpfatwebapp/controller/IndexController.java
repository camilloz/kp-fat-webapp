package kpfatwebapp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kpfatwebapp.config.Utils;
import kpfatwebapp.entity.User;
import kpfatwebapp.service.RoleService;
import kpfatwebapp.service.UserService;

@Controller
public class IndexController {

	@Autowired
	UserService userService;

	@Autowired
	RoleService userProfileService;

	@Autowired
	MessageSource messageSource;

	@RequestMapping(value = { "/", "/index" })
	public String index(Model model) {
		model.addAttribute("user", Utils.getPrincipal());
		return "/index";
	}
	
	@RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
	public String accessDeniedPage(ModelMap model) {
		model.addAttribute("user", Utils.getPrincipal());
		return "/accessDenied";
	}

	@RequestMapping(value = { "/registration" }, method = RequestMethod.GET)
	public String newUser(Model model) {
		User user = new User();
		model.addAttribute("newUser", user);
		return "/registration";
	}

	@RequestMapping(value = { "/registration" }, method = RequestMethod.POST)
	public String saveUser(@Valid @ModelAttribute("newUser") User newUser, BindingResult result) {
		if (!result.hasErrors()) {
			userService.save(newUser);
			return "redirect:/success";
		}
		return "/registration";
	}

	@RequestMapping(value = "/success", method = RequestMethod.GET)
	public String success() {
		return "/success";
	}

	@RequestMapping(value = { "/edituser" }, method = RequestMethod.GET)
	public String prepareUser(Model model) {
		User user = userService.findByEmail(Utils.getPrincipal());
		model.addAttribute("editableUser", user);
		return "/edituser";
	}

	@RequestMapping(value = { "/edituser" }, method = RequestMethod.POST)
	public String updateUser(@Valid @ModelAttribute("editableUser") User editableUser, BindingResult result) {
		if (!result.hasErrors()) {
			userService.updateUser(editableUser);
			return "redirect:/success";
		}
		return "/edituser";
	}
}
