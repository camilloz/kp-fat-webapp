package kpfatwebapp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kpfatwebapp.config.Utils;
import kpfatwebapp.entity.FilmShow;
import kpfatwebapp.entity.User;
import kpfatwebapp.service.FilmShowService;
import kpfatwebapp.service.RoleService;

@Controller
public class FilmShowController {

	@Autowired
	FilmShowService filmShowService;

	@Autowired
	RoleService userProfileService;

	@Autowired
	MessageSource messageSource;

	@Autowired
	private JavaMailSender mailSender;

	/**
	 * This method will provide the medium to add a new EMPTY user.
	 */
	@RequestMapping(value = { "/admin" }, method = RequestMethod.GET)
	public String newUser(Model model) {
		FilmShow filmShow = new FilmShow();
		model.addAttribute("newFilmShow", filmShow);
		return "/admin";
	}

	/**
	 * This method will be called on form submission, handling POST request for
	 * saving user in database. It also validates the user input
	 */
	@RequestMapping(value = { "/admin" }, method = RequestMethod.POST)
	public String saveUser(@Valid @ModelAttribute("newFilmShow") FilmShow newFilmShow, BindingResult result) {
		if (!result.hasErrors()) {
			filmShowService.save(newFilmShow);
			return "redirect:/filmshows";
		}
		return "/admin";
	}

	@RequestMapping(value = { "/filmshows" }, method = RequestMethod.GET)
	public String showAll(Model model) {
		model.addAttribute("listOfShows", filmShowService.findAll());
		return "/filmshows";
	}

	@RequestMapping(value = { "/filmshows/{id}" }, method = RequestMethod.GET)
	public String detail(Model model, @PathVariable int id) {
		model.addAttribute("chosenShow", filmShowService.findById(id));
		model.addAttribute("user", Utils.getPrincipal());
		return "/filmshowdetail";
	}

	@RequestMapping(value = { "/filmshows/{id}" }, method = RequestMethod.POST)
	public String someMethod(@RequestParam("nrOfTickets") Integer nrOfTickets, @PathVariable Integer id,
			@RequestParam("user") String recipient, @RequestParam("title") String title,
			@RequestParam("date") String date) {
		
		filmShowService.sellTickets(id, nrOfTickets);
		String recipientAddress = recipient;
		String subject = "Your tickets for KP Cinema";
		String message = "Hello, " + recipient + ",\n\nThis e-mail is a confirmation of your purchase. \nYou have bought " + 
		nrOfTickets + " tickets for: " + title + " on " + date + "\n\nRegards, \nKP Cinema!";

		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(recipientAddress);
		email.setSubject(subject);
		email.setText(message);

		mailSender.send(email);
		return "redirect:/success";
	}

	@RequestMapping(value = { "/filmshows" }, method = RequestMethod.POST)
	public String buyTicket(BindingResult result) {
		if (!result.hasErrors()) {
			return "redirect:/filmshows";
		}
		return "/filmshows";
	}
}
