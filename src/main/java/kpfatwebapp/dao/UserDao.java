package kpfatwebapp.dao;

import kpfatwebapp.entity.User;
 
public interface UserDao {
    User findById(int id);
    void save(User user);
    User findByEmail(String email);
}