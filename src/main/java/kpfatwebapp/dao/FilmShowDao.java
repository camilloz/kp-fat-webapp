package kpfatwebapp.dao;

import java.util.ArrayList;
import java.util.List;

import kpfatwebapp.entity.FilmShow;

public interface FilmShowDao {
    FilmShow findById(int id);
    void save(FilmShow filmShow);
    List<FilmShow> findAll();
    void sellTickets (Integer id, Integer numberOfTickets);
}