package kpfatwebapp.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kpfatwebapp.entity.User;

@Repository("userDao")
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void save(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.save(user);
	}

	@Override
	public User findById(int id) {
		Session session = sessionFactory.getCurrentSession();
		return (User) session.get(User.class, id);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public User findByEmail(String email) {
		Session session = sessionFactory.getCurrentSession();
		List<User> userList;
		userList = session.createQuery("from User u where u.email= :email").setString("email", email).list();
		if (userList.size() == 1) {
			return userList.get(0);
		} else {
			return null;
		}
	}
}