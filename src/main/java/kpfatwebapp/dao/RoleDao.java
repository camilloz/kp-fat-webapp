package kpfatwebapp.dao;

import kpfatwebapp.entity.Role;
 
 
public interface RoleDao {
    Role findById(int id);
    Role findByName(String name);
    void save(Role role);
    void populate();
}