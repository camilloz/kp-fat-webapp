package kpfatwebapp.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kpfatwebapp.aspect.AspectLogger;
import kpfatwebapp.entity.FilmShow;

@Repository("filmshowDao")
public class FilmShowDaoImpl implements FilmShowDao {
	private Logger logger = Logger.getLogger(AspectLogger.class);
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public FilmShow findById(int id) {
		Session session = sessionFactory.getCurrentSession();
		return (FilmShow) session.get(FilmShow.class, id);
	}

	@Override
	public void save(FilmShow filmShow) {
		Session session = sessionFactory.getCurrentSession();
		session.save(filmShow);
	}

	@Override
	public List<FilmShow> findAll() {
		Session session = sessionFactory.getCurrentSession();
		ArrayList<FilmShow> filmShowList;
		filmShowList = (ArrayList<FilmShow>) session.createQuery("from FilmShow").list();
		return filmShowList;
	}

	@Override
	public void sellTickets(Integer id, Integer numberOfTickets) {
		Session session = sessionFactory.getCurrentSession();
		ArrayList<FilmShow> filmShowList;
		filmShowList = (ArrayList<FilmShow>) session.createQuery("from FilmShow fs where fs.id = :id").setParameter("id", id).list();
		if (filmShowList.size() == 1) {
			FilmShow fs = filmShowList.get(0);
			fs.setNumberOfTicketsAvailable(fs.getNumberOfTicketsAvailable() - numberOfTickets);
		} else {
			logger.error("query for FilmShow result FAILED");
		}
	}
}