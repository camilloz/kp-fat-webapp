package kpfatwebapp.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import kpfatwebapp.entity.Role;
import kpfatwebapp.entity.User;

@Repository("roleDao")
public class RoleDaoImpl implements RoleDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Role findById(int id) {
		Session session = sessionFactory.getCurrentSession();
		return (Role) session.get(Role.class, id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Role findByName(String name) {
		Session session = sessionFactory.getCurrentSession();
		List<Role> roleList;
		roleList = session.createQuery("from Role r where r.name = :rolename").setString("rolename", name).list();
		if (roleList.size() == 1) {
			return roleList.get(0);
		} else {
			return null;
		}
	}

	@Override
	public void save(Role role) {
		Session session = sessionFactory.getCurrentSession();
		session.save(role);
	}
	
	@Override
	@Transactional
	public void populate() {
		Session session = sessionFactory.getCurrentSession();
		if (findByName("ADMIN") == null || findByName("USER") == null) {
			Role roleUser = new Role();
			roleUser.setName("USER");
			session.save(roleUser);
			
			Role roleAdmin = new Role();
			roleAdmin.setName("ADMIN");
			session.save(roleAdmin);
		}
	}
}