package kpfatwebapp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import kpfatwebapp.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/applicationContext.xml" }) 
@TransactionConfiguration(transactionManager="transactionManager")

public class UserServiceTest {
	@Autowired
	UserService userService;
	
	Integer expectedUserId;
	String expectedEmail;
	
	@Before
	public void setup() {
		expectedUserId = 1;
		expectedEmail = "admin@kp.pl";
	}
	
	@Test
	public void testServiceFindsUserProperlyByEmail () {
		Integer idFromService = userService.findByEmail(expectedEmail).getId();
		Assert.assertEquals(expectedUserId, idFromService);
	}
	
	@Test
	public void testServiceFindsUserProperlyById () {
		String emailFromService = userService.findById(expectedUserId).getEmail();
		Assert.assertEquals(expectedEmail, emailFromService);
	}
}
