package kpfatwebapp;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import kpfatwebapp.entity.User;
import kpfatwebapp.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/applicationContext.xml" }) 
@TransactionConfiguration(transactionManager="transactionManager")
@Transactional
public class UserRegistrationTest {
	@Autowired
	UserService userService;
	
	@Test
	public void testUserRegistration () {
		User user = new User();
		user.setEmail("testowy@jakasdomena.com");
		user.setFirstName("Jan");
		user.setLastName("Tester");
		user.setPassword("secret");
		userService.save(user);
		Assert.assertEquals(user.getEmail(), userService.findByEmail("testowy@jakasdomena.com").getEmail());
	}
}
